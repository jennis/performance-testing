#!/bin/sh
mkdir -p results
LOGNAME=$(hostname)-$(date "+%s").json
PLAF=$(uname -s | tr A-Z a-z)
ELEMENTS="test-set/elements/a*"
./hyperfine.$PLAF --warmup 2 --export-json "results/$LOGNAME" -m 5 \
	'id -u' \
	'python3 -c"import testbase"' \
	'bst help' \
	'find '"$ELEMENTS"' -name "*.bst"' \
	'find '"$ELEMENTS"' -name "*.bst" | xargs stat -t' \
	'find '"$ELEMENTS"' -name "*.bst" | python3 test-stat.py' \
	'find '"$ELEMENTS"' -name "*.bst" | xargs cat' \
	'find '"$ELEMENTS"' -name "*.bst" | python3 test-read.py' \
	'find '"$ELEMENTS"' -name "*.bst" | xargs sha256sum' \
	'find '"$ELEMENTS"' -name "*.bst" | python3 test-sha.py' \
	'find '"$ELEMENTS"' -name "*.bst" | python3 test-parse.py'
echo "Written results to $LOGNAME"
